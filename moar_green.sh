# /bin/bash
date +"%Y-%m-%d, %R" >> $HOME/projects/moar_green/dates.log
# das alles soll jetzt nur noch geschehen, wenn eine gewisse condition an die
# dates erfüllt ist.
curdate=$(date +%d.%m.%y)
if cat $HOME/projects/moar_green/list_chillout_days | grep -w "$curdate"; then
    git --git-dir=$HOME/projects/moar_green/.git --work-tree=$HOME/projects/moar_green add $HOME/projects/moar_green/dates.log
    git --git-dir=$HOME/projects/moar_green/.git --work-tree=$HOME/projects/moar_green commit -m "moar green"
    git --git-dir=$HOME/projects/moar_green/.git --work-tree=$HOME/projects/moar_green push origin main
else
    echo "skipped bc not in list" >> $HOME/projects/moar_green/cron.log
    echo $curdate >> $HOME/projects/moar_green/cron.log
    echo "skipped"
fi
